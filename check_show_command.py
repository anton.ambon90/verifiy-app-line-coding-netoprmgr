import os
import re




cisco_ios = [
    'show inventory', 'show running-config',
    'show version', 
    'show process cpu', 'show process mem', 
    'show log', 'show env', 'show env all',
    'show environment', 'show environment all',
    ]
cisco_xe = [
    'show inventory', 'show running-config',
    'show ver',  
    'show process cpu', 'show process mem', 
    'show log', 'show env', 'show env all',
    'show environment', 'show environment all',
    ]


def verify_command_ios():
            
                # global  cisco_capture
                
                #Periksa Capture

                verify_lists = []
            
                for file in os.listdir('capture/') :

                    with open('capture/' + file) as f:
                        hasil = f.read() 
                        sh_inven = re.findall('show\s+inventory', hasil)
                        sh_rn_cg = re.findall('show running-config', hasil)
                        sh_version = re.findall('show version', hasil)
                        sh_ver = re.findall('show\sver', hasil)
                        sh_pr_cpu = re.findall('show process cpu', hasil)
                        sh_pr_mem = re.findall('show process mem', hasil)
                        sh_log = re.findall('show log', hasil)
                        sh_env = re.findall('show env', hasil)
                        sh_env_all = re.findall('show env all', hasil)
                        sh_envmt = re.findall('show environment', hasil)
                        sh_envmt_all = re.findall('show environment all', hasil)
     
                        sh_cisco_ios = (sh_inven + sh_rn_cg + sh_version + sh_pr_cpu + sh_pr_mem + sh_log + sh_env + sh_env_all + sh_envmt + sh_envmt_all)

                        # sh_cisco_ios = (f'{sh_inven}-{sh_rn_cg}-{sh_ver}-{sh_version}-{sh_pr_cpu}--{sh_pr_mem}--{sh_log}--{sh_env}--{sh_env_all}--{sh_envmt}--{sh_envmt_all}')
                        # sh_cisco_xe = (f'{sh_inven}-{sh_rn_cg}-{sh_ver}--{sh_pr_cpu}--{sh_pr_mem}--{sh_log}--{sh_env}--{sh_env_all}--{sh_envmt}--{sh_envmt_all}')
                
                        
                        for ios in cisco_ios :
                            if (ios in sh_cisco_ios ) :
                                verify_lists.append({
                                    "filename_capture": file,
                                    "verified_status": "Verified",
                                    "Command_capture": ios, 
                                })
                    
                            else:
                                verify_lists.append({
                                "filename_capture": file,
                                "verified_status": "Not Verified",
                                "Command_capture": ios,
                                })  
                                
                return verify_lists
                    

# verify_commands = verify_command_ios()
# for verify_capture in verify_commands:
#     print(verify_capture)
    
    
def verify_command_xe():
            
                # global  cisco_capture
                
                #Periksa Capture
                verify_lists = []
            
                for file in os.listdir('capture/') :

                    with open('capture/' + file) as f:
                        hasil = f.read() 
                        sh_inven = re.findall('show\s+inventory', hasil)
                        sh_rn_cg = re.findall('show running-config', hasil)
                        sh_version = re.findall('show version', hasil)
                        sh_ver = re.findall('show\sver', hasil)
                        sh_pr_cpu = re.findall('show process cpu', hasil)
                        sh_pr_mem = re.findall('show process mem', hasil)
                        sh_log = re.findall('show log', hasil)
                        sh_env = re.findall('show env', hasil)
                        sh_env_all = re.findall('show env all', hasil)
                        sh_envmt = re.findall('show environment', hasil)
                        sh_envmt_all = re.findall('show environment all', hasil)
     
                        sh_cisco_xe = (sh_inven + sh_rn_cg + sh_ver + sh_version + sh_pr_cpu + sh_pr_mem + sh_log + sh_env + sh_env_all + sh_envmt + sh_envmt_all)
                        

                        # sh_cisco_ios = (f'{sh_inven}-{sh_rn_cg}-{sh_ver}-{sh_version}-{sh_pr_cpu}--{sh_pr_mem}--{sh_log}--{sh_env}--{sh_env_all}--{sh_envmt}--{sh_envmt_all}')
                        # sh_cisco_xe = (f'{sh_inven}-{sh_rn_cg}-{sh_ver}--{sh_pr_cpu}--{sh_pr_mem}--{sh_log}--{sh_env}--{sh_env_all}--{sh_envmt}--{sh_envmt_all}')
                
                        
                        for xe in cisco_xe :
                            if (xe in sh_cisco_xe ) :
                                verify_lists.append({
                                    "filename_capture": file,
                                    "verified_status_capture": "Verified",
                                    "Command_capture": xe, 
                                })
                    
                            else:
                                verify_lists.append({
                                "filename_capture": file,
                                "verified_status_capture": "Not Verified",
                                "Command_capture": xe,
                                })  
                                
                        return  verify_lists    
                                
# verify_commands = verify_command_xe()
# for verify_capture in verify_commands:
#     print(verify_capture)